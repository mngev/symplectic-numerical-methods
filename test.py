#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  Запуск фортран программы с различными параметрами для тестирования численных методов
#-------------------------------------------------------------------------------
import subprocess
import sys
import time
from numpy import array
from colorama import Fore, Style
import pandas as pd
import matplotlib.pyplot as plt
#
# Все имеющиеся реализации методов
#
RK_implementations = [ 'RKp2n1', 'RKp2n2', 'RKp3n1', 'RKp3n2', 'RKp3n3', 'RKp4n1', 'RKp4n2', 'RKp4n3', 'RKp5n1', 'RKp5n2', 'RKp6n1' ]
SPRK_implementations = [ 'SPRKs1p1n1', 'SPRKs3p3n1', 'SPRKs3p3n2', 'SPRKs4p4n1', 'SPRKs4p4n2', 'SPRKs6p4n1', 'SPRKs6p4n2' ]
ERK_implementations = [ 'ERK3(2)', 'ERK4(3)', 'Fehlberg_ERK4(5)', 'Cash-Karp_ERK4(5)', 'Dormand–Prince_ERK5(4)', 'Fehlberg_ERK7(8)', 'DVERK_ERK6(5)' ]
SRKN_implementations = [ 'SRKNs5p4', 'SRKNs5p5', 'SRKNs17p8' ]
#
# Все возможные тесты
#
tests = [
    {
        'numerical method' : 'RK',
        'discription' : "Классический метод Рунге-Кутты",
        'implementations' : RK_implementations
    },
    {
        'numerical method' : 'ERK',
        'discription' : "Вложенный метод Рунге-Кутты",
        'implementations' : ERK_implementations
    },
    {
        'numerical method' : 'SPRK',
        'discription': "Симплектический раздельный метод Рунге-Кутты",
        'implementations' : SPRK_implementations
    },
    {
        'numerical method' : 'SRKN',
        'discription': "Симплектический метод Рунге-Кутты-Нюстрёма",
        'implementations' : SRKN_implementations
    }
]
program = './test'
#-------------------------------------------------------------------------------
# Тестируем время выполнения
#-------------------------------------------------------------------------------
if(sys.argv[1] == 'time'):
    # Что тестировать
    to_test = [ 'RK', 'ERK' ]
    # Шаблоны для распечатки
    template = "| {0:25} | {1:<20} |"
    up_bar = "{:-^52}\n".format('-')
    low_bar = "\n{:-^52}".format('-')
    #---------------------------------
    for test in tests:
        if test['numerical method'] in to_test:
            print(Fore.GREEN + test['discription'] + Style.RESET_ALL)
            print(up_bar + template.format("Реализация","Время вычисления") + low_bar)
            for implementation in test['implementations']:
                t1 = time.time()
                subprocess.call([program, 'time', test['numerical method'], implementation])
                t2 = time.time()
                print(template.format(implementation, t2-t1))
            print(up_bar)

#-------------------------------------------------------------------------------
# Тестируем глобальные погрешности
#-------------------------------------------------------------------------------
if(sys.argv[1] == 'error'):
    # Что тестировать
    to_test = [ 'RK', 'ERK' ]
    # Шаблоны для распечатки
    template = "| {0:25} | {1[0]:^20} | {1[1]:^25} |"
    up_bar = "{:-^80}\n".format('-')
    low_bar = "\n{:-^80}".format('-')
    #---------------------------------
    for test in tests:
        if test['numerical method'] in to_test:
            print(Fore.GREEN + test['discription'] + Style.RESET_ALL)
            print(up_bar + template.format("Реализация",["t_n", "||x(t_n)-x_n||"]) + low_bar)
            for implementation in test['implementations']:
                # Запускаем программу
                proc = subprocess.Popen([program, 'error', test['numerical method'], implementation],stdout = subprocess.PIPE)
                # На выходе у нас только одна строка
                output = proc.communicate()[0]
                x = array(output.decode("utf-8").split(), dtype='float64')
                print(template.format(implementation, x))
            print(up_bar)
#-------------------------------------------------------------------------------
# Тестируем свойства симплектичности
#-------------------------------------------------------------------------------
if(sys.argv[1] == 'symplectic'):
    # Что тестировать
    to_test = [ 'SPRK', 'RK', 'ERK', 'SRKN' ]
    # Шаблоны для распечатки
    template = "| {0:25} | {1:30} | {2:^28} |"
    invariants = [ 'Полная энергия', 'Момент импульса', 'Вектор Лапласа-Рунге-Ленца']
    up_bar = "{:-^93}\n".format('-')
    low_bar = "\n{:-^93}".format('-')
    #---------------------------------
    for test in tests:
        if test['numerical method'] in to_test:
            print(Fore.GREEN + test['discription'] + Style.RESET_ALL)
            print(up_bar + template.format("Реализация","Инвариант", "Погрешность") + low_bar)
            for implementation in test['implementations']:
                # Запускаем программу
                proc = subprocess.Popen([program, 'symplectic', test['numerical method'], implementation],stdout = subprocess.PIPE)
                # На выходе у нас теперь три строки, так как задача 2-х тел имеет 4-х мерное фазовое пространство
                output = proc.communicate()[0]
                for (invariant, error) in zip(invariants, output.splitlines()):
                    print(template.format(implementation, invariant, error.decode("utf-8")))
                print(up_bar,end='')
            print(up_bar)
#-------------------------------------------------------------------------------
#  Рисуем график зависимости погрешности от размера шага
#-------------------------------------------------------------------------------
if(sys.argv[1] == 'step_error'):
    # Что тестировать (только классический метод Рунге-Кутты)
    to_test = [ 'RK' ]
    csv_file = 'RK_step_error.csv'
    datafile = open(csv_file,'w')
    for test in tests:
        if test['numerical method'] in to_test:
            print("Тестируем зависимость погрешности от величины\n шага для классических методов Рунге-Кутты")
            for implementation in test['implementations']:
                i = test['implementations'].index(implementation) + 1
                print('\r[{0}>] {1} из {2}'.format('='*(3*i), i, len(RK_implementations) ),end="")
                # Запускаем программу
                subprocess.call([program, 'step_error', test['numerical method'], implementation], stdout = datafile)
    datafile.close()
    print("\nCSV-файл с данными по каждому методу сгенерирован")
    
    # Готовим полотно для рисования
    fig = plt.figure(0)
    ax =  fig.add_subplot(1,1,1)
    ax.set_title('Погрешность методов Рунге-Кутты в зависимости от шага', fontsize=10)
    ax.set_xlabel('Размер шага $h$', fontsize=14)
    ax.set_ylabel('Величина абсолютной глобальной погрешности', fontsize=14)
    ax.set_yscale('log')
    # Теперь считываем данные, используя в качестве индекса первую колонку
    # присваивая имена 'Step','Error' второй и третей колонкам и конвертируя их
    # в тип float64
    table = pd.read_csv(
                        csv_file,
                        index_col=0,
                        header=0,
                        names=['Step','Error'],
                        dtype={'Step':'float64', 'Error':'float64'}
                       )
    # И делаем группировку по первой колонке
    for test in tests:
        if test['numerical method'] in to_test:
            for implementation in test['implementations']:
                ax.plot(
                        table.loc[implementation]['Step'].values,
                        table.loc[implementation]['Error'].values,
                        label = implementation
                       )
                ax.legend(loc='lower right', ncol=2)
    fig.savefig('RK_step_error.png' , dpi = 400, format = 'png', pad_inches=0.1)
    print("График построен")

    








