module functions
implicit none
!-------------------------------------------------------------------------------
! Интерфейс функции, реализующей правую часть ОДУ
!-------------------------------------------------------------------------------
abstract interface
  function f(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: f
  end function f
end interface
contains
!-------------------------------------------------------------------------------
!  Функции для проверки глобальной ошибки
! у данной системы ОДУ есть точное аналитическое решение
!-------------------------------------------------------------------------------
  function func(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: func
    !==
    func(1) = -1.0*(x(1) + 5.0*x(2))
    func(2) = x(1) + x(2)
  end function func
  ! Точное решение для func
  function solution(y,t)
    implicit none
    double precision, dimension(:), intent(in) :: y
    double precision, intent(in) :: t
    double precision, dimension(1:ubound(y,1)) :: solution
    !==
    solution(1) = -0.5*(5.0*y(2) + y(1))*sin(2.0*t) + y(1)*cos(2.0*t)
    solution(2) = y(2)*cos(2.0*t) + 0.5*(y(1) + y(2))*sin(2.0*t)
  end function solution
!-------------------------------------------------------------------------------
!  Гармонический осциллятор
!-------------------------------------------------------------------------------
  function oscillator(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: oscillator
    !==
    oscillator(1) = -1.0*x(2)
    oscillator(2) =  x(1)
  end function oscillator
  ! Точное решение для oscillator
  function oscillator_exact(x,t)
    implicit none
    double precision, dimension(:), intent(in) :: x
    double precision, intent(in) :: t
    double precision, dimension(1:ubound(x,1)) :: oscillator_exact
    !== Начальные значения (xo, yo)
    oscillator_exact(1) = x(2)*cos(t) - x(1)*sin(t)
    oscillator_exact(2) = x(1)*cos(t) + x(2)*sin(t)
  end function oscillator_exact
!-------------------------------------------------------------------------------
!  Функции для проверки симплектического раздельного метода Рунге-Кутты
! ОДУ для задачи Кеплера
!-------------------------------------------------------------------------------
  ! Для обычных методов
  function Kepler(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: Kepler
    Kepler(1) = -1.0d0*x(3)/(sqrt(x(3)**2 + x(4)**2)**3)
    Kepler(2) = -1.0d0*x(4)/(sqrt(x(3)**2 + x(4)**2)**3)
    Kepler(3) = x(1)
    Kepler(4) = x(2)
  end function Kepler
  ! 1) Две функции для симплектических методов
  function Kepler_f(x,p)
    implicit none
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: Kepler_f
    
    Kepler_f(1) = -1.0d0*x(1)/(sqrt(x(1)**2 + x(2)**2)**3)
    Kepler_f(2) = -1.0d0*x(2)/(sqrt(x(1)**2 + x(2)**2)**3)
  end function Kepler_f
  ! 2)
  function Kepler_g(x,p)
    implicit none
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: Kepler_g
    
    Kepler_g(1) = x(1) 
    Kepler_g(2) = x(2)
  end function Kepler_g
  ! Функция Гамильтона для задачи двух тел
  pure function KeplerH(x)
    implicit none
    double precision, dimension(:), intent(in) :: x
    double precision :: KeplerH
    KeplerH = 0.5D0*(x(1)**2 + x(2)**2) - 1.0d0/sqrt(x(3)**2 + x(4)**2)
  end function KeplerH
  ! Момент импульса для задачи двух тел
  pure function KeplerM(x)
    implicit none
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(1:ubound(x,1)) :: KeplerM
    
    KeplerM(1) = 0.0d0
    KeplerM(2) = 0.0d0
    KeplerM(3) = x(3)*x(2) - x(4)*x(1)
  end function KeplerM
  ! Вектор Лаплпса--Рунге--Ленца для задачи двух тел
  pure function KeplerW(x)
    implicit none
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(1:ubound(x,1)) :: KeplerW
    ! момент импульса (нужна третья компонента)
    double precision, dimension(1:ubound(x,1)) :: M
    
    M = KeplerM(x)
    
    KeplerW(1) =  x(2)*M(3) - x(3)/sqrt(x(3)**2 + x(4)**2)
    KeplerW(2) = -x(1)*M(3) - x(4)/sqrt(x(3)**2 + x(4)**2)
    KeplerW(3) = 0.0d0
  end function KeplerW
!-------------------------------------------------------------------------------
!  Связанные осцилляторы (также для проверки симплектических методов)
!-------------------------------------------------------------------------------
  ! Для обычных методов
  function coupled_oscillators(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: coupled_oscillators
    ! Коэффициента пропорциональности и масса
    double precision :: k, m

    k = p(1)
    m = p(2)

    coupled_oscillators(1) = -k*x(3) - k*(x(3) - x(4))
    coupled_oscillators(2) = -k*x(4) + k*(x(3) - x(4))
    coupled_oscillators(3) = x(1)/m
    coupled_oscillators(4) = x(2)/m
  end function coupled_oscillators
  ! Для симплектических методов
  function coupled_oscillators_f(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: coupled_oscillators_f
    ! Коэффициента пропорциональности и масса
    double precision :: k, m

    k = p(1)
    m = p(2)

    coupled_oscillators_f(1) = -k*x(1) - k*(x(1) - x(2))
    coupled_oscillators_f(2) = -k*x(2) + k*(x(1) - x(2))
  end function coupled_oscillators_f
  !=======
  function coupled_oscillators_g(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: coupled_oscillators_g
    ! Коэффициента пропорциональности и масса
    double precision :: k, m

    k = p(1)
    m = p(2)

    coupled_oscillators_g(1) = x(1)/m
    coupled_oscillators_g(2) = x(2)/m
  end function coupled_oscillators_g
  ! Гамильтониан для связанных осцилляторов
  pure function coupled_oscillatorsH(x,p)
    implicit none
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision :: coupled_oscillatorsH
    ! Коэффициента пропорциональности и масса
    double precision :: k, m
    k = p(1)
    m = p(2)
    coupled_oscillatorsH = (x(1)**2 + x(2)**2)/(2.0d0*m) + &
                  (k/2.0d0)*(x(3)**2 + x(4)**2) + (k/2.0d0)*(x(3)-x(4))**2
  end function coupled_oscillatorsH

!-------------------------------------------------------------------------------
!  Функция, задающая правую часть ОДУ брюсселятора (Brusselator)
!-------------------------------------------------------------------------------
  function Brusselator(t,x,p)
    implicit none
    ! x(1) = 1.5
    ! x(2) = 3.0
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: Brusselator
    !==
    Brusselator(1) = 1.0d0 + x(2)*x(1)**2 - 4.0d0*x(1)
    Brusselator(2) = 3.0d0*x(1) - x(2)*x(1)**2
  end function Brusselator
!-------------------------------------------------------------------------------
!  Аттрактор Лоренца (трехмерный фазовый портрет)
!-------------------------------------------------------------------------------
  function Lorenz(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: Lorenz
    
    double precision :: sigma
    double precision :: rho
    double precision :: beta
    !==
    sigma = p(1)
    rho = p(2)
    beta = p(3)
    
    Lorenz(1) = sigma*(x(2) - x(1))
    Lorenz(2) = x(1)*(rho - x(3)) - x(2)
    Lorenz(3) = x(1)*x(2) - beta*x(3)
  end function Lorenz
!-------------------------------------------------------------------------------
!  Модель конкурент симбионт конкурент симбионт
!-------------------------------------------------------------------------------
  function cscs(t,x,p)
    implicit none
    double precision, intent(in) :: t
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: cscs
    
    
    cscs(1) = 300.0d0 * x(1) * (1.0d0 - x(1)/1500.0d0) - 14.0d0 * x(1) * x(2) / (9.0d0 * (1.0d0 + x(3) / 270.0d0))
    cscs(2) = 400.0d0 * x(3) * (1.0d0 - x(3) / (1150.0d0 + 4.0 * x(1) / 90.5d0))
    cscs(3) = 250.0d0 * x(2) * (1.0d0 - x(2) / 1000.0d0) - (x(1) * x(2)) / (9.0d0 * (1.0 + x(4) / 475.0d0))
		cscs(4) = 450.0d0 * x(4) * (1.0d0 - x(4) / (2394.0d0 + 76.0d0 * x(2) / 875.0d0))
  end function cscs
end module functions