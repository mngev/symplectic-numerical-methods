!-------------------------------------------------------------------------------
!              Раздельные симплектические методы Рунге-Кутты
!-------------------------------------------------------------------------------
module SPRKs
implicit none
private
!-------------------------------------------------------------------------------
! Набор глобальных переменных, задающих раздельный метод Рунге--Кутты
!-------------------------------------------------------------------------------
integer                                       :: method_stage
integer                                       :: method_order
integer                                       :: equations_number
double precision, allocatable, dimension(:)   :: b
double precision, allocatable, dimension(:)   :: hat_b
double precision, allocatable, dimension(:,:) :: XX
double precision, allocatable, dimension(:,:) :: YY	
! Флаг включения отладочных сообщений
logical, parameter                            :: debug = .false.
!-------------------------------------------------------------------------------
!                  Доступные вовне подпрограммы и функции
!-------------------------------------------------------------------------------
public :: SPRK
!-------------------------------------------------------------------------------
!  Абстрактный интерфейс для функции, представляющей правую часть системы ОДУ
!-------------------------------------------------------------------------------
abstract interface
  function ode_func(x,p)
    double precision, dimension(:), intent(in) :: x ! Переменные
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: ode_func
  end function ode_func
end interface
contains
!-------------------------------------------------------------------------------
!                        Инициализация всех коэффициентов
!-------------------------------------------------------------------------------
  subroutine CoefficientsInit(method_code_name)
    character(len = *), intent(in) :: method_code_name
    select case (trim(method_code_name))              
!-------------------------------------------------------------------------------
    case('SPRKs1p1n1')
    ! Симплектический метод Эйлера
      method_stage = 1
      method_order = 1
      
      allocate( b(1:method_stage), hat_b(1:method_stage) )
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,1:(method_stage+1)) )
      
      b(1) = 1.0d0
      hat_b(1) = 1.0d0              
!-------------------------------------------------------------------------------
    case('SPRKs3p3n1')
    ! Раздельный метод РК (s = 3 и p = 3) (Sanz–Serna)
      method_stage = 3
      method_order = 3
      
      allocate( b(1:method_stage), hat_b(1:method_stage) )
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,1:(method_stage+1)) )
      
      b = [ 0.268330d0, -0.187992d0, 0.919662d0 ]
      hat_b = [ 0.919662d0, -0.187992d0, 0.268330d0 ]            
!-------------------------------------------------------------------------------
    case('SPRKs3p3n2')
    ! Раздельный метод РК (s = 3 и p = 3) (Ruth)
      method_stage = 3
      method_order = 3
      
      allocate( b(1:method_stage), hat_b(1:method_stage) )
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,1:(method_stage+1)) )
      
      b = [ 7.0d0/24.0d0, 3.0d0/4.0d0, -1.0d0/24.0d0 ]
      hat_b = [ 2.0d0/3.0d0, -2.0d0/3.0d0, 1.0d0 ]            
!-------------------------------------------------------------------------------
    case('SPRKs4p4n1')
    !  Раздельный метод РК (s = 4 и p = 4) (Candy Razmus)
      method_stage = 4
      method_order = 4
      
      allocate( b(1:method_stage), hat_b(1:method_stage) )
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,1:(method_stage+1)) )
      
      b(1) = (2.0d0 + 2.0d0**(1.0d0/3.0d0) + 1.0d0/2.0d0**(1.0d0/3.0d0))/6.0d0
      b(2) = (1.0d0 - 2.0d0**(1.0d0/3.0d0) - 1.0d0/2.0d0**(1.0d0/3.0d0))/6.0d0
      b(3) = (1.0d0 - 2.0d0**(1.0d0/3.0d0) - 1.0d0/2.0d0**(1.0d0/3.0d0))/6.0d0
      b(4) = (2.0d0 + 2.0d0**(1.0d0/3.0d0) + 1.0d0/2.0d0**(1.0d0/3.0d0))/6.0d0
      hat_b(1) = 0.0d0
      hat_b(2) = 1.0d0/(2.0d0 - 2.0d0**(1.0d0/3.0d0))
      hat_b(3) = 1.0d0/(1.0d0 - 4.0d0**(1.0d0/3.0d0))
      hat_b(4) = 1.0d0/(2.0d0 - 2.0d0**(1.0d0/3.0d0))
!-------------------------------------------------------------------------------
    case('SPRKs4p4n2') !  <-- Дает погрешность! Проверить!
    !  Раздельный метод РК (s = 4 и p = 4) (Forest, Ruth)
      method_stage = 4
      method_order = 4
      
      allocate( b(1:method_stage), hat_b(1:method_stage) )
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,1:(method_stage+1)) )
      
      b(1) = 0.0d0
      b(2) = (1.0d0 + 2.0d0**(1.0d0/3.0d0) + 1.0d0/2.0d0**(1.0d0/3.0d0))/3.0d0
      b(3) = (1.0d0 - 2.0d0**(1.0d0/3.0d0) - 1.0d0/2.0d0**(1.0d0/3.0d0))/3.0d0
      b(4) = (1.0d0 + 2.0d0**(1.0d0/3.0d0) + 1.0d0/2.0d0**(1.0d0/3.0d0))/3.0d0
      hat_b(1) = (1.0d0 + 2.0d0**(1.0d0/3.0d0) + 1.0d0/2.0d0**(1.0d0/3.0d0))/3.0d0
      hat_b(2) = (1.0d0 - 2.0d0**(1.0d0/3.0d0) - 1.0d0/2.0d0**(1.0d0/3.0d0))/6.0d0
      hat_b(3) = (1.0d0 - 2.0d0**(1.0d0/3.0d0) - 1.0d0/2.0d0**(1.0d0/3.0d0))/6.0d0
      hat_b(4) = (1.0d0 + 2.0d0**(1.0d0/3.0d0) + 1.0d0/2.0d0**(1.0d0/3.0d0))/3.0d0    
!-------------------------------------------------------------------------------
    case('SPRKs6p4n1')
    !  Раздельный метод РК (s = 6 и p = 4) (Sanz–Serna)
      method_stage = 6
      method_order = 4
      
      allocate( b(1:method_stage), hat_b(1:method_stage) )
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,1:(method_stage+1)) )
      
      b = [ 0.134165d0, -0.093996d0, 0.459831d0, 0.459831d0, -0.093996d0, 0.134165d0 ]
      hat_b = [ 0.459831d0, -0.093996d0, 0.268330d0, -0.093996d0, 0.459831d0, 0.0d0 ]              
!-------------------------------------------------------------------------------
    case('SPRKs6p4n2')
    ! Раздельный метод РК (s = 6 и p = 4) (Okunbor, Skeel)
      method_stage = 6
      method_order = 4
      
      allocate( b(1:method_stage), hat_b(1:method_stage) )
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,1:(method_stage+1)) )
      
      b = [ 7.0d0/48.0d0, 3.0d0/8.0d0, -1.0d0/48.0d0, -1.0d0/48.0d0, 3.0d0/8.0d0, 7.0d0/48.0d0 ]
      hat_b = [ 1.0d0/3.0d0, -1.0d0/3.0d0, 1.0d0, -1.0d0/3.0d0, 1.0d0/3.0d0, 0.0d0 ]
    
    end select
  end subroutine CoefficientsInit
!-------------------------------------------------------------------------------
!             Один шаг симплектического раздельного метода Рунге-Кутты
!                         (для внутреннего использования)
!-------------------------------------------------------------------------------
  subroutine SPRKMethodStep(f, g, x, y, p1, p2, h)
    implicit none  
      procedure(ode_func)                                             :: f
      procedure(ode_func)                                             :: g
      double precision, dimension(1:equations_number), intent(inout)  :: x
      double precision, dimension(1:equations_number), intent(inout)  :: y
      double precision, dimension(:), intent(in)                      :: p1
      double precision, dimension(:), intent(in)                      :: p2
      double precision, intent(in)                                    :: h
    !---------------------------------------------------------------------------
      integer                                                         :: s
      integer                                                         :: EQN
      integer                                                         :: i
    !---------------------------------------------------------------------------
      s = method_stage
      EQN = equations_number
      ! XX = 0.0d0
      ! YY = 0.0d0
    ! {--------- Раздельный симплектический метод Рунге-Кутта ------------------
      XX(1:EQN,0) = x(1:EQN)
      YY(1:EQN,1) = y(1:EQN)
      do i = 1,s,1
        XX(1:EQN,i) = XX(1:EQN,i-1) + h*b(i)*f(YY(1:EQN,i), p1)
        YY(1:EQN,i+1) = YY(1:EQN,i) + h*hat_b(i)*g(XX(1:EQN,i), p2)
      end do
      x(1:EQN) = XX(1:EQN,s)
      y(1:EQN) = YY(1:EQN,s+1)
    ! -------------------------------------------------------------------------}
  end subroutine SPRKMethodStep
!-------------------------------------------------------------------------------
! Проверка переданных аргументов на адекватность (возвращает 0 в случае успех)
!-------------------------------------------------------------------------------
 pure function ArgumentsChecker(t, time_interval)
    implicit none
    double precision, intent(in) :: t
    double precision, intent(in) :: time_interval(1:2)
    double precision             :: t_start, t_stop
    integer                      :: ArgumentsChecker

    t_start = time_interval(1)
    t_stop  = time_interval(2)

    if( t >= t_stop) then
      ArgumentsChecker = -1
    else if(t_start >= t_stop) then
      ArgumentsChecker = -2
    else if (t < t_start ) then
      ArgumentsChecker = -3
    else
      ArgumentsChecker = 0
    end if  
 end function ArgumentsChecker
!===============================================================================
!        Симплектический раздельный метод Рунге--Кутты, полный цикл
!===============================================================================
  subroutine SPRK(f,g, EQN, x, y, t, p1, p2, time_interval, h, stdout, method)
    implicit none
    procedure(ode_func)                               :: f
    procedure(ode_func)                               :: g
    integer, intent(in)                               :: EQN
    double precision, dimension(1:EQN), intent(inout) :: x
    double precision, dimension(1:EQN), intent(inout) :: y
    double precision, intent(inout)                   :: t
    double precision, dimension(:), intent(in)        :: p1 
    double precision, dimension(:), intent(in)        :: p2
    double precision, dimension(1:2), intent(in)      :: time_interval
    double precision, intent(in)                      :: h
    logical, intent(in)                               :: stdout
    character(len = *), optional, intent(in)          :: method
  !-----------------------------------------------------------------------------
    character(len = 32)                               :: method_code_name
    character(len = 32)                               :: format_string
    character(len = 10)                               :: columns_number
  !-----------------------------------------------------------------------------
  ! Проверка параметров  
    if ( ArgumentsChecker(t,time_interval) /= 0 ) error stop "Arguments error!"
    
  ! Инициализируем все глобальные переменные
    equations_number = EQN ! Число уравнений в каждой из двух систем
    method_code_name = trim(adjustl(method))
    
    call CoefficientsInit(method_code_name)
  
  ! Формат распечатки результатов
    write(columns_number,'(i10)'), 2*equations_number 
    format_string = '(SP,'//trim(adjustl(columns_number))//'(e24.17,","),1e24.17)'
    
    if(stdout) then
  ! {------------ Симплектический раздельный метод Рунге-Кутта -----------------
      do while(t <= time_interval(2))
        write(*,format_string) t, x, y
        call SPRKMethodStep(f, g, x, y, p1, p2, h)
        t = t + h
      end do ! while
    else
      do while(t <= time_interval(2))
        call SPRKMethodStep(f, g, x, y, p1, p2, h)
        t = t + h
      end do ! while
  ! ---------------------------------------------------------------------------}
    end if
    deallocate(b,hat_b,XX,YY)
  end subroutine SPRK
end module SPRKs