!-------------------------------------------------------------------------------
!              Симплектические методы Рунге-Кутты-Нюстрёма
!-------------------------------------------------------------------------------
module SRKNs
implicit none
private
!-------------------------------------------------------------------------------
! Набор глобальных переменных, задающих раздельный метод Рунге--Кутты
!-------------------------------------------------------------------------------
integer                                       :: method_stage
integer                                       :: method_order
integer                                       :: equations_number
double precision, allocatable, dimension(:)   :: b
double precision, allocatable, dimension(:)   :: c
double precision, allocatable, dimension(:,:) :: XX
double precision, allocatable, dimension(:,:) :: YY	
! Флаг включения отладочных сообщений
logical, parameter                            :: debug = .false.
!-------------------------------------------------------------------------------
!                  Доступные вовне подпрограммы и функции
!-------------------------------------------------------------------------------
public :: SRKN
!-------------------------------------------------------------------------------
!  Абстрактный интерфейс для функции, представляющей правую часть системы ОДУ
!-------------------------------------------------------------------------------
abstract interface
  function ode_func(x,p)
    double precision, dimension(:), intent(in) :: x ! Переменные
    double precision, dimension(:), intent(in) :: p ! Параметры
    double precision, dimension(1:ubound(x,1)) :: ode_func
  end function ode_func
end interface
contains
!-------------------------------------------------------------------------------
!                        Инициализация всех коэффициентов
!-------------------------------------------------------------------------------
  subroutine CoefficientsInit(method_code_name)
    character(len = *), intent(in) :: method_code_name
    select case (trim(method_code_name))              
!-------------------------------------------------------------------------------
    case('SRKNs5p4')
    ! "Метод Рунге-Кутты-Нюстрёма (s = 5 и p = 4)"
      method_stage = 5
      method_order = 4
      
      allocate(b(1:method_stage),c(0:method_stage))
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,0:method_stage) )
      
      b = [ 0.0617588581356263250d0, 0.3389780265536433551d0, &
            0.6147913071755775662d0, -0.1405480146593733802d0, &
            0.1250198227945261338d0 ]
      c = [ 0.0d0, 0.0d0, 0.2051776615422863869d0, &
            0.6081989431465009739d0, 0.4872780668075869657d0, 1.0d0 ]
!-------------------------------------------------------------------------------
    case('SRKNs5p5')
    ! Метод Рунге-Кутты-Нюстрёма (s = 5 p = 5) (Okunbor Skeel)
      method_stage = 5
      method_order = 5
      
      allocate(b(1:method_stage),c(0:method_stage))
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,0:method_stage) )
      b = [ -1.67080892327314312060d0, 1.22143909230997538270d0, &
             0.08849515813253908125d0, 0.95997088013770159876d0, &
             0.40090379269297793385d0 ]
      c = [ 0.0d0, 0.69491389107017931259d0, 0.63707199676998338411d0, &
            -0.02055756998211598005d0, 0.79586189634575355001d0, &
            0.30116624272377778837d0 ]
!-------------------------------------------------------------------------------
    case('SRKNs17p8')
    ! "Метод Рунге-Кутты-Нюстрёма (s = 17 p = 8) (Okunbor Lu)"
      method_stage = 17
      method_order = 8
      
      allocate(b(1:method_stage),c(0:method_stage))
      allocate( XX(1:equations_number,0:method_stage) )
      allocate( YY(1:equations_number,0:method_stage) )
      b(9) = 0.26632861379051880890d0
      !--
      b(8) = 0.21297732182168671589d0
      b(10) = b(8)
      !--
      b(7) = -0.00610812346737106375d0
      b(11) = b(7)
      !--
      b(6) = -0.85399694134759518427d0
      b(12) = b(6)
      !--
      b(5) = 0.00406786380988635211d0
      b(13) = b(5)
      !--
      b(4) = 0.65600968737291831534d0
      b(14) = b(4)
      !--
      b(3) = -0.51659631611408440843d0
      b(15) = b(3)
      !--
      b(2) = 0.53305963988632443229d0
      b(16) = b(2)
      !--
      b(1) = 0.33742256114297547454d0
      b(17) = b(1)
      !==============================
      c(9) = 0.50000000000000000000d0
      !--
      c(10) = 0.98859812152448378608d0
      c(8) = 1.0d0 - c(10)
      !--
      c(11) = 0.24213736883372802278d0
      c(7) = 1.0d0 - c(11)
      !--
      c(12) = 0.91488346152470712003d0
      c(6) = 1.0d0 - c(12)
      !--
      c(13) = 0.37691184022396195807d0
      c(5) = 1.0d0 - c(13)
      !--
      c(14) = 0.89680911596755130688d0
      c(4) = 1.0d0 - c(14)
      !--
      c(15) = -0.10016008223569197733d0
      c(3) = 1.0d0 - c(15)
      !--
      c(16) = -0.09593931237138830781d0
      c(2) = 1.0d0 - c(16)
      !--
      c(17) = 0.80617999817902952220d0
      c(1) = 1.0d0 - c(17)
      c(0) = 0.0d0
    end select
  end subroutine CoefficientsInit
!-------------------------------------------------------------------------------
!             Один шаг симплектического метода Рунге-Кутты-Нюстрёма
!                         (для внутреннего использования)
!-------------------------------------------------------------------------------
  subroutine SRKNMethodStep(f, g, x, y, p1, p2, h)
    implicit none  
      procedure(ode_func)                                             :: f
      procedure(ode_func)                                             :: g
      double precision, dimension(1:equations_number), intent(inout)  :: x
      double precision, dimension(1:equations_number), intent(inout)  :: y
      double precision, dimension(:), intent(in)                      :: p1
      double precision, dimension(:), intent(in)                      :: p2
      double precision, intent(in)                                    :: h
    !---------------------------------------------------------------------------
      integer                                                         :: s
      integer                                                         :: EQN
      integer                                                         :: i
    !---------------------------------------------------------------------------
      s = method_stage
      EQN = equations_number
      ! XX = 0.0d0
      ! YY = 0.0d0
    ! {----------- Симплектический метод Рунге-Кутта-Нюстрёма ------------------
      XX(1:EQN,0) = x(1:EQN)
      YY(1:EQN,0) = y(1:EQN)
      do i = 1,s,1
        YY(1:EQN,i) = YY(1:EQN,i-1) + h*(c(i)-c(i-1))*g(XX(1:EQN,i-1), p2)
        XX(1:EQN,i) = XX(1:EQN,i-1) + h*b(i)*f(YY(1:EQN,i), p1)
      end do
      x(1:EQN) = XX(1:EQN,s)
      y(1:EQN) = YY(1:EQN,s) + h*(1.0d0 - c(s))*XX(1:EQN,s)
    ! -------------------------------------------------------------------------}
  end subroutine SRKNMethodStep
!-------------------------------------------------------------------------------
! Проверка переданных аргументов на адекватность (возвращает 0 в случае успех)
!-------------------------------------------------------------------------------
 pure function ArgumentsChecker(t, time_interval)
    implicit none
    double precision, intent(in) :: t
    double precision, intent(in) :: time_interval(1:2)
    double precision             :: t_start, t_stop
    integer                      :: ArgumentsChecker

    t_start = time_interval(1)
    t_stop  = time_interval(2)

    if( t >= t_stop) then
      ArgumentsChecker = -1
    else if(t_start >= t_stop) then
      ArgumentsChecker = -2
    else if (t < t_start ) then
      ArgumentsChecker = -3
    else
      ArgumentsChecker = 0
    end if  
 end function ArgumentsChecker
!===============================================================================
!        Симплектический метод Рунге--Кутты-Нюстрёма, полный цикл
!===============================================================================
  subroutine SRKN(f,g, EQN, x, y, t, p1, p2, time_interval, h, stdout, method)
    implicit none
    procedure(ode_func)                               :: f
    procedure(ode_func)                               :: g
    integer, intent(in)                               :: EQN
    double precision, dimension(1:EQN), intent(inout) :: x
    double precision, dimension(1:EQN), intent(inout) :: y
    double precision, intent(inout)                   :: t
    double precision, dimension(:), intent(in)        :: p1 
    double precision, dimension(:), intent(in)        :: p2
    double precision, dimension(1:2), intent(in)      :: time_interval
    double precision, intent(in)                      :: h
    logical, intent(in)                               :: stdout
    character(len = *), optional, intent(in)          :: method
  !-----------------------------------------------------------------------------
    character(len = 32)                               :: method_code_name
    character(len = 32)                               :: format_string
    character(len = 10)                               :: columns_number
  !-----------------------------------------------------------------------------
  ! Проверка параметров  
    if ( ArgumentsChecker(t,time_interval) /= 0 ) error stop "Arguments error!"
    
  ! Инициализируем все глобальные переменные
    equations_number = EQN ! Число уравнений в каждой из двух систем
    method_code_name = trim(adjustl(method))
    
    call CoefficientsInit(method_code_name)
  
  ! Формат распечатки результатов
    write(columns_number,'(i10)'), 2*equations_number 
    format_string = '(SP,'//trim(adjustl(columns_number))//'(e24.17,","),1e24.17)'
    
    if(stdout) then
  ! {------------- Симплектический метод Рунге-Кутта-Нюстрема ------------------
      do while(t <= time_interval(2))
        write(*,format_string) t, x, y
        call SRKNMethodStep(f, g, x, y, p1, p2, h)
        t = t + h
      end do ! while
    else
      do while(t <= time_interval(2))
        call SRKNMethodStep(f, g, x, y, p1, p2, h)
        t = t + h
      end do ! while
  ! ---------------------------------------------------------------------------}
    end if
    deallocate(b,c,XX,YY)
  end subroutine SRKN
end module SRKNs