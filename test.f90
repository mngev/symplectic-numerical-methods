!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!                Программа для тестирования численных методов
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
program test
  use functions
  use RKs
  use ERKs
  use SPRKs
  use SRKNs
  implicit none
!-------------------------------------------------------------------------------  
  integer, parameter          ::     EQN = 2
  double precision, parameter ::       h = 0.001
  double precision, parameter ::       e = 1.0e-8
  double precision, parameter ::   A_tol = 1.0e-8
  double precision, parameter ::   R_tol = 1.0e-8
  double precision, parameter :: t_start = 0.0
  double precision, parameter ::  t_stop = 1000.0
  double precision, dimension(1:2), parameter  :: y_o = [ 1.0d0, 1.0d0 ] ! Для осциллятора
  ! Для задачи двух тел
  double precision, dimension(1:4), parameter  :: z_o = [ -0.9048d0, 0.03317d0, 0.3862d0, 1.0315d0 ]
!-------------------------------------------------------------------------------
  double precision, dimension(1:2) :: t_interval
  
  double precision :: step  ! Шаг, который можно менять
  double precision :: t     
!-------------------------------------------------------------------------------
  double precision, dimension(1:3)            :: p    ! Параметры функции
  double precision, dimension(1:2)             :: x    ! Сеточная функция
  double precision, dimension(1:2)             :: y    ! Сеточная функция
  double precision, dimension(1:4)             :: z    ! Сеточная функция
!-------------------------------------------------------------------------------
  character(len = 64)                 :: format_string
  character(len = 32), dimension(0:4) :: arg ! Аргументы командной строки
  integer :: i
!-------------------------------------------------------------------------------
  
  t_interval = [ t_start, t_stop ]
  t = t_start
  y = y_o
  
  ! Считываем все аргументы
  do i = 1,command_argument_count(),1
    call get_command_argument(i, arg(i))
  end do
  
!-------------------------------------------------------------------------------
!                           Вычисление глобальной ошибки
!-------------------------------------------------------------------------------
  if(trim(arg(1)) == 'error') then
  ! {---
    select case(trim(arg(2)))
      case('RK')
        call RK(oscillator, EQN, y, t, p, t_interval, h, trim(arg(3)))
        write(*,*) t, euclid_norm(oscillator_exact(y_o, t) - y)
        
      case('ERK')
        call ERK(oscillator, EQN, y, t, p, t_interval, e, e, .false., trim(arg(3)))
        write(*,*) t, euclid_norm(oscillator_exact(y_o, t) - y)
    end select
  !---}      
  end if
!-------------------------------------------------------------------------------
!                        Тест на время выполнения
!-------------------------------------------------------------------------------
  if(trim(arg(1)) == 'time') then
  ! {---
    select case( trim(arg(2)) )
      case('RK') ! Метод Рунге-Кутта (подпрограмма с полным циклом)
        call RK(oscillator, EQN, y, t, p, t_interval, h, trim(arg(3)))
        
      case('ERK') ! Вложенные методы Рунге-Кутта с коррекцией шага
        call ERK(oscillator, EQN, y, t, p, t_interval, e, e, .false., trim(arg(3)))
    end select
  !---}  
  end if
!-------------------------------------------------------------------------------
! Тест на зависимость погрешности от шага классического метода Рунге-Кутта
!-------------------------------------------------------------------------------
  if(trim(arg(1)) == 'step_error') then
    step = 0.0001
    t_interval = [ 0.0d0, 100.0d0 ]
    format_string = '(SP, 1A6, ",", 1e24.17, ",", 1e24.17)'
    do while(step <= 1.0d0)
      t = t_start
      y = y_o
      call RK(oscillator, EQN, y, t, p, t_interval, step, trim(arg(3)))
      write(*,format_string) trim(arg(3)), step, euclid_norm(oscillator_exact(y_o, t) - y)
      step = step + 0.0001
    end do ! while
  end if
!-------------------------------------------------------------------------------
! Тест на симплектичность (находится погрешность вычисления инвариантов)
!-------------------------------------------------------------------------------
  if(trim(arg(1)) == 'symplectic') then
  ! {---
    select case ( trim(arg(2)) )
      case('SPRK')
        x = z_o(1:2) ! Импульс
        y = z_o(3:4) ! Координаты
        call SPRK(Kepler_f, Kepler_g, 2, x, y, t, p, p, t_interval, h, .false., trim(arg(3)))
        z(1:2) = x
        z(3:4) = y
        write(*,*) abs(KeplerH(z_o) - KeplerH(z))
        write(*,*) euclid_norm(KeplerM(z_o) - KeplerM(z))
        write(*,*) euclid_norm(KeplerW(z_o) - KeplerW(z))
      case('SRKN')
        x = z_o(1:2) ! Импульс
        y = z_o(3:4) ! Координаты
        call SRKN(Kepler_f, Kepler_g, 2, x, y, t, p, p, t_interval, h, .false., trim(arg(3)))
        z(1:2) = x
        z(3:4) = y
        write(*,*) abs(KeplerH(z_o) - KeplerH(z))
        write(*,*) euclid_norm(KeplerM(z_o) - KeplerM(z))
        write(*,*) euclid_norm(KeplerW(z_o) - KeplerW(z))
      case('RK')
        z = z_o
        call RK(Kepler, 4, z, t, p, t_interval, h, trim(arg(3)))
        write(*,*) abs(KeplerH(z_o) - KeplerH(z))
        write(*,*) euclid_norm(KeplerM(z_o) - KeplerM(z))
        write(*,*) euclid_norm(KeplerW(z_o) - KeplerW(z))
      
      case('ERK')
        z = z_o
        call ERK(Kepler, 4, z, t, p, t_interval, e, e, .false., trim(arg(3)))
        write(*,*) abs(KeplerH(z_o) - KeplerH(z))
        write(*,*) euclid_norm(KeplerM(z_o) - KeplerM(z))
        write(*,*) euclid_norm(KeplerW(z_o) - KeplerW(z))
    end select
  !---}  
  end if
end program test