#! /usr/bin/env python2
# -*- coding: utf-8 -*-
from __future__ import print_function
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import animation

Lorenz = {
	'model_name' : "Аттрактор Лоренца",
	'file_name' : "Lorenz",
	'time' : ['t'],
	'variables' : ['x','y','z'],
	'parameters' : ['sigma','rho','beta'],
	'param_list' : np.arange(15,30,0.1),
	'cmd' : ['./main','Lorenz','ERK','Dormand–Prince_ERK5(4)']
}

to_animation  = [ Lorenz ]

# Создаем окно и оси координат
fig = plt.figure(0)
ax = fig.gca(projection='3d')

# Функция инициализации: рисует фон будущей анимированной картинки
def init3d():
	line.set_data([], [])
	line.set_3d_properties([])
	return line,

# Функция анимации: вызывается по счетчику i, каждое i --- один кадр
def animate3d(i, data, line):
	x = data[i][:,0]
	y = data[i][:,1]
	z = data[i][:,2]
	# Подпись из-за чего-то "плывет"
	#ax.set_title("Rho = "+str(0.2*i))
	line.set_data(x, y)
	line.set_3d_properties(z)
	return line,

for M in to_animation:
	csv_file = M['file_name']+".csv"
	table = pd.read_csv(csv_file)
	grouped = table.groupby(M['parameters'][1])
	d = []
	for name, group in grouped:
		d.append(group[M['variables']].values)
		
	ax.set_axis_off()
	ax.set_xlim(table['x'].min(), table['x'].max())
	ax.set_ylim(table['y'].min(), table['y'].max())
	ax.set_zlim(table['z'].min(), table['z'].max())
	l, = ax.plot([], [], [], linewidth=0.5)
	
	anim = animation.FuncAnimation(fig, animate3d, frames=len(d), interval=1, blit=True, fargs=(d, l)) 
	anim.save(M['model_name']+'.mp4', fps=25)