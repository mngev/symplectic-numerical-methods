#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
from numpy import loadtxt
import matplotlib.pyplot as plt


def qp(plots):
    '''Задача функции нарисовать двухмерный график по информации из файла csv
        Образец словаря для функции:
        'data' : file.csv
        'x' : [ 0 ],
        'y' : [ 1, 2 ], # Какие колонки использовать для графиков?
        'title' : "Название рисунка",
        'legend' : [ "Название первого графика", "Название второго графика" ],
        'xlabel' : "Подпись оси x",
        'ylabel' : "Подпись оси y",
        'file_name' : "Название файла, в котором будет сохранена картинка",
        'line_type' : ['-', ':'],
        'color' : '#669966'
        'file_format' : 'pdf' '''
    fig = plt.figure(0)
    ax =  fig.add_subplot(1,1,1)
    
    ax.set_title(plots['title'])
    ax.set_xlabel(plots['xlabel'])
    ax.set_ylabel(plots['ylabel'])
    
    data = loadtxt(plots['data'], delimiter=',', unpack=True)
    
    for (pl, lg, ls) in zip(plots['y'], plots['legend'], plots['line_type']):
        ax.plot(
                data[plots['x']],
                data[pl],
                linestyle=ls,
                color=plots['color'],
                label = lg
                )
        ax.legend()
    fig.savefig(plots['file_name'], dpi = 400, format=plots['file_format'], pad_inches=0.1)
    fig.close()
    return 0