# Компилятор
FC = gfortran
# Параметры компилятора -fdefault-real-8
FFLAGS = -fcheck=bounds -c -fbacktrace -ffpe-trap=invalid,zero,overflow,denormal
# НазваниЯ главных программ
TEST = test
CALC = calculations
# Исходные файлы подключаемых модулей находятся в каталоге SRCDIR
SRCDIR = ./src
LIBS = $(wildcard $(SRCDIR)/*.f90)
#LAPACK = $(shell pkg-config --libs lapack)
# Объектные файлы библиотек
OBJS = $(LIBS:.f90=.o)
# Директория для хранения скомпилированных o и mod файлов
ODIR = ./modules
# Файл для вычисленных данных
DAT_FILE = data.dat
# Временные файлы
CLEAN = *.o *.png *.mod *.zip *.log *~ *.pdf *.dat *.csv *.out

all: buildtest buildcalcule
# Компиляция главной программы
buildtest: modules $(TEST).o
	-@$(FC) -cpp $(addprefix $(ODIR)/,$(notdir $(OBJS)) $(TEST).o)  -o $(TEST)
buildcalcule: modules $(CALC).o
	-@$(FC) -cpp $(addprefix $(ODIR)/,$(notdir $(OBJS)) $(CALC).o)  -o $(CALC)
# Создание объектных файлов
modules: $(OBJS)
# правило по созданию объектных файлов из исходных текстов
# f90
%.o: %.f90
	@echo -e "Компилируется файл: \033[1;31m" $< "\033[1;37m"
	-@mkdir -p $(ODIR)
	-@$(FC) $(FFLAGS) $< -o $(ODIR)/$(notdir $@) -J $(ODIR)/
# f77
%.o: %.f
	@echo -e "Компилируется файл: \033[0;31m" $< "\033[1;37m"
	-@mkdir -p $(ODIR)
	-@$(FC) $(FFLAGS) $< -o $(ODIR)/$(notdir $@)  -J $(ODIR)/

# Тест методов (локальная и глобальная погрешности)
test:
	./test.py time

calc:
	./calculations > $(DAT_FILE)

clean:
	@rm -vf $(CLEAN) $(TEST) $(CALC)
	@rm -vf $(ODIR)/*
arch: clean
	zip RKs.zip *
