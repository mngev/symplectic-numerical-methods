#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

def H(x,y,p1,p2):
    return  0.5*(p1**2 + p2**2) - 1.0/np.sqrt(x**2 + y**2)

t,x,y,p1,p2 = np.loadtxt('data.dat', delimiter=',', unpack=True)

Ham = H(x,y,p1,p2)

fig = plt.figure(0)
ax =  fig.add_subplot(1,1,1)

ax.set_title("Название графика", fontsize=14)
ax.set_xlabel("Ось x", fontsize=14)
ax.set_ylabel("Ось y", fontsize=14)

ax.plot(t, Ham, linewidth=0.5)
#ax.legend()
fig.savefig("График.png" , dpi = 400, format = 'png', pad_inches=0.1)