!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!        Программа для решения систем ОДУ численными методами
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
program calculations
  use functions
  use RKs
  use ERKs
  use SPRKs
  implicit none
!-------------------------------------------------------------------------------  
  integer, parameter :: EQN = 2
  integer, parameter :: EQN2 = 4
  double precision, parameter ::       h = 0.1
  double precision, parameter ::       e = 0.0000001
  double precision, parameter :: t_start = 0.0
  double precision, parameter ::  t_stop = 200.0
  double precision, dimension(1:2), parameter  :: x_o = [ -0.9048d0, 0.03317d0 ] ! Импульс
  double precision, dimension(1:2), parameter  :: y_o = [ 0.3862d0, 1.0315d0 ] ! Координаты
!-------------------------------------------------------------------------------
  double precision, dimension(1:2) :: t_interval
  double precision :: t  
!-------------------------------------------------------------------------------
  double precision, dimension(1:2)            :: p1   ! Параметры функции
  double precision, dimension(1:2)            :: p2   ! Параметры функции
  double precision, dimension(1:2)             :: x    ! Сеточная функция
  double precision, dimension(1:2)             :: y    ! Сеточная функция
  double precision, dimension(1:4)             :: z    ! Сеточная функция
!-------------------------------------------------------------------------------
  character(len = 64)                 :: format_string
  character(len = 32), dimension(0:4) :: arg ! Аргументы командной строки
  integer :: i
!-------------------------------------------------------------------------------

  p1 = 0.0d0
  p2 = 0.0d0
  x = x_o
  y = y_o
  t = t_start
	! Задача Кеплера
  !z = [ -0.9048d0, 0.03317d0, 0.3862d0, 1.0315d0 ]
	! Модель конкурент-симбионт-конкурент-симбионт
	z = [ 250.0d0, 1250.0d0, 1250.0d0, 2500.0d0 ]
  t_interval = [ t_start, t_stop ]
  ! SPRKs1p1n1 SPRKs3p3n1 SPRKs3p3n2 SPRKs4p4n1 SPRKs4p4n2 SPRKs6p4n1 SPRKs6p4n2
  
  !call SPRK(Kepler_f, Kepler_g, EQN, x, y, t, p1, p2, t_interval, h, .true., 'SPRKs3p3n1')
  
  !call RK(cscs, EQN2, z, t, p1, t_interval, h, 'RKp4n1')
   
  call ERK(cscs, EQN2, z, t, p1, t_interval, e, e, .true., 'DVERK_ERK6(5)')
end program calculations