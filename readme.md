## Структура каталогов

В каталоге `src` находятся следующие файлы

- `RKs.f90` — классический метод Рунге--Кутты с постоянным шагом,
- `ERKs.f90` — вложенный метод Рунге--Кутты с переменным шагом,
- `SPRKs.f90` — симплектический раздельный метод Рунге--Кутты,
- `SRKNs.f90` — симплектический метод Рунге--Кутты--Нюстрёма,
- `functions.f90` — функции, реализующие правую часть различных гамильтоновых уравнений.

В коренном каталоге

- `calculations.f90` — расчет некоторых задач,
- `test.f90` — тестирование погрешностей,
- `test.py` — запуск тестирования погрешностей и обработка результатов.

## Аргументы процедуры

 - `func` —  функция, задающая правую часть системы ОДУ,
 - `EQN` — размерность системы,
 - `y` — функция y (сеточная функция),
 - `t` — время,
 - `p` — параметры функции,
 - `t_start`, `t_stop` — временной промежуток численного интегрирования,
 - `h` — шаг метода,
 - `method` — название метода (необязательный аргумент).

## Параметры метода

 - `s` — стадийность метода,
 - `order` — порядок метода,
 - `m_name` — название метода (для внутреннего использования).

## Переменные

 - `i`, `alpha` —  счетчики,
 - `g` — вспомогательная функция,
 - `a`, `b`, `c`, `k` — коэффициенты метода Рунге--Кутта.
-----------------------------------------------------------------------------